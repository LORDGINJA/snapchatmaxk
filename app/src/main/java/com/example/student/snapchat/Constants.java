package com.example.student.snapchat;

/**
 * Created by Student on 05/12/16.
 */
public class Constants {
    public static final String ACTION_ADD_FRIEND = "com.example.student.snapchat.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.student.snapchat.SEND_FRIEND_REQUEST";
    public static final String ACTION_SEND_PHOTO = "com.example.student.snapchat.SEND_PHOTO";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.student.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.student.snapchat.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.student.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.student.snapchat.ADD_FRIEND_FAILURE";


}
