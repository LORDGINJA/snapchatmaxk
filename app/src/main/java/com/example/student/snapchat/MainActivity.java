package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "101E7176-C915-1721-FF9B-7984585DF500";
    public static final String SECRET_KEY = "699BC2AA-31B6-C933-FFDE-94BE3B93A100";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        } else {
            LoggedInFragment loggedIn = new LoggedInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }
    }

}
